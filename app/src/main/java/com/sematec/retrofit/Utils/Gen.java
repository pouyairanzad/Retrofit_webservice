package com.sematec.retrofit.Utils;

import android.util.Log;
import android.widget.Toast;

import com.sematec.retrofit.BuildConfig;
import com.sematec.retrofit.EndPoints;
import com.sematec.retrofit.Genretor;

public class Gen {
    public static String YAhooBaseURL = "https://query.yahooapis.com/v1/public/" ;
    public static String IpApiURL="http://ip-api.com";

    public static EndPoints endPoints =
            Genretor.createService(EndPoints.class) ;
    public  static com.sematec.retrofit.IPAPI.EndPoints endPoints2 =
            com.sematec.retrofit.IPAPI.Genretor.createService( com.sematec.retrofit.IPAPI.EndPoints.class );




    public static void toast(String msg) {
        Toast.makeText(BaseApplication.app, msg, Toast.LENGTH_SHORT).show();
    }

    public static void log(String tag, String msg) {
        if (BuildConfig.DEBUG)
            Log.d(tag, msg);
    }

    public static void log(String msg) {
        if (BuildConfig.DEBUG)
            Log.d("debug_log", msg);
    }
}
