package com.sematec.retrofit.Utils;

import android.app.Application;
import android.graphics.Typeface;

public class BaseApplication extends Application {
    public static BaseApplication app;
    public static Typeface appFace;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        appFace = Typeface.createFromAsset(getAssets(), "ir_sans.ttf");

    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
