package com.sematec.retrofit.MVPWeather;

import com.sematec.retrofit.DTO.YahooModel;

public interface Contract {
    interface  View{
        void onWeatherReceived(YahooModel model);
        void failed(String msg);
        void ShowDilog(boolean show);

    }
    interface  Presenter{
        void Attachview(View view);
        void getWeather(String query) ;
        void onWeatherReceived(YahooModel model);
        void failed(String msg);
    }
    interface  Model{
        void AttachPresenter(Presenter presenter);
        void getWeather(String query);

    }
}
