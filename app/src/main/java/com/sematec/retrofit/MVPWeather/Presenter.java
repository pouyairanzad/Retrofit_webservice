package com.sematec.retrofit.MVPWeather;

import com.sematec.retrofit.DTO.YahooModel;

public class Presenter implements Contract.Presenter {
    private Contract.View view;
    Contract.Model model = new Model();
    @Override
    public void Attachview(Contract.View view) {
        this.view=view;
        model.AttachPresenter( this );

    }

    @Override
    public void getWeather(String city) {
        model.getWeather( city );
        view.ShowDilog(true);



    }

    @Override
    public void onWeatherReceived(YahooModel model) {
        view.onWeatherReceived( model );
        view.ShowDilog(false);

    }

    @Override
    public void failed(String msg) {
        view.failed( msg );
        view.ShowDilog(false);

    }
}
