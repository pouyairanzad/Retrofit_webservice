package com.sematec.retrofit.MVPWeather;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.sematec.retrofit.DTO.YahooModel;
import com.sematec.retrofit.R;
import com.sematec.retrofit.Utils.BaseActivity;
import com.sematec.retrofit.Utils.Gen;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity (R.layout.activity_wheater_mvpactivity)

public class WheaterMVPACtivity extends BaseActivity implements Contract.View {
    Contract.Presenter presenter =new Presenter();

    @ViewById
    EditText city;
    @ViewById
    Button show;
    @ViewById
    TextView result;


    @AfterViews
    void init(){presenter.Attachview( this );}
    @Click
    void show(){
        presenter.getWeather( city.getText().toString() );

    }



    @Override
    public void onWeatherReceived(YahooModel model) {
        result.setText( model.getQuery().getResults().getChannel().getItem().getCondition().getTemp() );

    }

    @Override
    public void failed(String msg) {
        Gen.toast(msg );

    }

    @Override
    public void ShowDilog(boolean show) {
        if(show)
            dialog.show();
        else
            dialog.dismiss();

    }
}
