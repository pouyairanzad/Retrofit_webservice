package com.sematec.retrofit;

import com.sematec.retrofit.DTO.YahooModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface EndPoints {
    @GET("yql/")
    Call<YahooModel> getWeather(
            @Query("q") String query ,
            @Query("format") String format
    ) ;
}
