package com.sematec.retrofit;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.sematec.retrofit.Utils.BaseActivity;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );
    }
}
