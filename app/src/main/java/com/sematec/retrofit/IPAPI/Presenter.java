package com.sematec.retrofit.IPAPI;

public class Presenter implements Contract.Presenter {

    private Contract.View view;
    Contract.Model model = new Model();

    @Override
    public void AttachView(Contract.View view) {

        this.view = view;
        model.AttachPresenter( this );
        model.getDetail();
    }

    @Override
    public void onReceiveDetail(IPAPIModel model) {
        view.onReceiveDetail( model );

    }
}
