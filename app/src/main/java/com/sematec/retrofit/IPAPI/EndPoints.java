package com.sematec.retrofit.IPAPI;

import retrofit2.Call;
import retrofit2.http.GET;

public interface EndPoints {
    @GET ("json")
    Call<IPAPIModel> getDetails( ) ;

}
