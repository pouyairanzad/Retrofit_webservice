package com.sematec.retrofit.IPAPI;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Model implements  Contract.Model {
    private Contract.Presenter presenter;
    @Override
    public void AttachPresenter(Contract.Presenter presenter) {
        this.presenter=presenter;


    }

    @Override
    public void getDetail() {
        Genretor.createService(EndPoints.class  ).getDetails().enqueue( new Callback<IPAPIModel>() {
            @Override
            public void onResponse(Call<IPAPIModel> call, Response<IPAPIModel> response) {
                presenter.onReceiveDetail(  response.body());


            }

            @Override
            public void onFailure(Call<IPAPIModel> call, Throwable t) {

            }
        } );

    }
}
