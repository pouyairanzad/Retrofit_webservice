package com.sematec.retrofit.IPAPI;

import com.sematec.retrofit.Utils.Gen;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Genretor {
    public static <S> S createService(Class<S> serviceClass) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl( Gen.IpApiURL)
                .client(client)
                .addConverterFactory( GsonConverterFactory.create())
                .build();

        return retrofit.create(serviceClass);
    }
}
