package com.sematec.retrofit.IPAPI;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.sematec.retrofit.R;
import com.sematec.retrofit.Utils.BaseActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity (R.layout.activity_ipapiactivity )

public class IPAPIACtivity extends BaseActivity implements Contract.View
{
    Contract.Presenter presenter = new Presenter();
    @ViewById
    TextView result;

    @AfterViews
    void init(){
        presenter.AttachView( this );
    }


    @Override
    public void onReceiveDetail(IPAPIModel model) {
        result.setText( model.getCountry()+""+model.getCity() );

    }
}
