package com.sematec.retrofit.IPAPI;

public interface Contract {
    interface  View{
        void onReceiveDetail(IPAPIModel model) ;

    }
    interface Presenter{

        void AttachView(View view);
        void onReceiveDetail(IPAPIModel model) ;
    }
    interface  Model{
        void AttachPresenter(Presenter presenter);
        void getDetail() ;

    }
}
